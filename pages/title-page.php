<?php include(mpu_plugin_dir . "./pages/loadAnimation.php"); ?>
<div class="mainAdminPage">
	<style><?php include(mpu_plugin_dir . "./pages/style.css"); ?></style>
	<h1>Books &amp Papers Plugin Page</h1>
	<div class="InformationText">
		The plugin provides management tools for creation or edition of author and publication data.<br>
		<h2>Features</h2>
		<p>Use the left menu to select what you want to do.</p>
		<p><span class="marker">Add Author</span> and <span class="marker">Manage Authors</span> contain tools for author management.</p>
		<p><span class="marker">Add Work</span> and <span class="marker">Manage Works</span> contain tools for publication management.</p>
		<p><span class="marker">Import Paper</span> contain data import tools (using DOI or local <a href="https://en.wikipedia.org/wiki/BibTeX">bib</a> file).</p>
		<p>Data is displayed using special tags placed into post/page edit window.</p>
		<p>Use following structure on your page.</p>
		<p><span class="marker">[publications auth={author} subj={publication} prep={all/only/ex}]</span></p>
		<p>Here, <span class="marker">{author}</span> should be replaced 
		with the identifier of existing author (see the corresponding field in the author properties menu) 
		and <span class="marker">{publication}</span> should be replaced with publication type: <span class="marker">articles</span>, <span class="marker">conferences</span> or <span class="marker">books</span>.</p>
		<p>Both can be replaced with <span class="marker">all</span> to list all existing publications and/or publications from every author.</p>
		<p>Preprint option applies to articles only and should be <span class="marker">all</span>, <span class="marker">only</span>or <span class="marker">ex</span>, where:</p>
		<ul>
			<li></li><span class="marker">all</span> means all articles will be printed</li>
			<li><span class="marker">only</span> means only articles marked as &quotpreprint&quot will be printed</li>
			<li><span class="marker">ex</span> means only articles that are NOT marked will be printed</li>
		</ul>
		<p>For example, to list all articles of John Doe with johndoe identifier, use the following command:</p>
		<p><span class="marker">[publications auth=johndoe subj=articles prep=all]</span></p>
		<p>See more details on the <a href="https://gitlab.com/engraver/sci-bib-online">plugin page</a> and its Wiki.</p>
	</div>
</div>
<script>
	window.onload = function () {
		if(document.getElementById("loadAnimWrapper")){
			document.getElementById("loadAnimWrapper").style = "display:none;";
		}
	}
</script>
