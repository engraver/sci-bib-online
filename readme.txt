=== Books & Papers ===
Contributors: frier, engraver
Donate link: none
Tags: academic, publication, paper, article, conference, proceedings, bibliography, management, auto-fill, autofill
Requires at least: 5.0.3
Tested up to: 5.0.3
Stable tag: 0.05
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Plugin provides a simple bibliography management tool for collecting and 
providing online access to list of publication of scientists, writers and 
people who manages collections of publications.

== Description ==

Using Books & Papers plugin one can manage lists of authors and their publications 
(books, journal publications like scientific papers/[arXiv](https://arxiv.org/) preprints and conference proceedings). 
Additional features are available like import via [DOI](http://dx.doi.org/) and [BibTeX](http://www.bibtex.org/) database.

User fills database records, provides author identifiers and then can display lists of publications by their type for each
author in the given page of website.

=== Author management ===
Each author is characterized by the following fields:
* First name
* Last name
* E-mail: john@doe.com
* Personal URL: https://doe.com/john, author name becomes a link if URL is provided
* Identifier: internal identifier used for referencing the concrete person (johndoe, jonny1 etc).

First of all, authors can be added manually. Authors are added during import of external publication. 
If they are not recognized automaticallly among already existing ones, new records are created. You can
merge duplicate records later as well as modify existing ones.

=== Publications ===
Three types of publications are supported: 
* Article is a journal publication like [this one](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.99.054422). 
It is characterized by list of authors (may be selected from existing records as well as just mentioned as external ones), 
title, journal, link to supplementary materials, date for sorting etc.
* Conference work is characterized mainly with the same fields as Artcile with additional Book Title field.
* Book is a publication, also characterized by editors, publisher, chaprter (for the referencing separate chapters in a textbook).

One can provide custom characteristics (max. three characteristics are supported) displaying by special stylesheet for the selected
record. 

=== Settings ===
You can configure some of the plugin functionallity:
* Set database prefix for tables that store author and publication data.
* Name the custom characteristic and set the style which it uses.
* Set the timeout of success/failure notification.

=== Usage ===
Let suppose, you've added an author named John Doe with johndoe identifier. 
There are 10 Article records, 3 Book records and 8 Conference records are related with John Doe. One Article is marked as preprint.
Then, the following command are accessible:
* [publications auth=johndoe subj=articles prep=all] will show all 10 Articles incluing preprint.
* [publications auth=johndoe subj=articles prep=only] will show only 1 record (preprint)
* [publications auth=johndoe subj=articles prep=ex] will show 9 Articles without preprint.
* [publications auth=johndoe subj=books] will show 3 books 
* [publications auth=johndoe subj=conferences] will show 8 conference records.

Settings menu provides fields for customization:
* Headers for articles, conferences and books sections.
* Substitution list for flexible records output format. For example, command 
[authors] <i>[title]</i>, [journal] {{Vol. <b>[volume]</b>}} [pages] ([year]) will result in a list of 
authors, italic title of the article, journal title, bold volume number, pages number and article year in parentheses. 
Note, that {{...}} syntax means that this part of record will not be displayed if [volume] is not provided.

== Installation ==

1. Install the plugin usind WordPress plugins screen or upload the plugin and paste its content to the `/wp-content/plugins/books-and-papers` directory.
1. Activate the plugin on the 'Plugins' screen in WordPress.
1. Go to Settings->Books and Papers screen and configure the plugin.

== Frequently Asked Questions ==

= How can I access the plugin? =

Plugin menus are located on the left admin panel.

= Plugin menu doesn't show up. =

Plugin must be enabled first. Go to 'Plugins'->'Installed Plugins' and enable the plugin.

= How can the list of publications be shown on the site? =

Use the following tag: [publications auth=author subj=publication], where 'author' should be replaced with author slug or 'all' for everyone, and 'publication' replaced with paper  type (articles, conferences or books) or 'all' for any type. For example: [publications auth=all subj=articles] will shown articles of every author.

= No publications are shown, while tag is correct. =

Possible reasons:
* Plugin is not enabled.
* No publications were entered. Go to 'Manage Papers' submenu to check. It should have a list of all publications.
* Publications are not public. Go to 'Manage Papers' submenu, find needed publication, click 'Modify', 'Paper is public' should be checked.

= I want to emphasise some publications on list. =

You can set up custom characteristic on the plugin settings page: name it and enter its style. Then modify the publication by checking the charactertic.

= What I should enter as custom characteristic's style? =

The style field should contain style description similar to HTML style attribute. For example, to make white text on black background, enter "color:white,background-color:black" (without quotes). Find more options online, searching for "style attribute properties".

== Screenshots ==

1. No description.

== Changelog ==

= 1.0 =
* Initial release.

== Upgrade Notice ==

= 1.0 =
Initial release.
