<?php
/* 
Plugin Name: Books & Papers
Plugin URI: 
Version: 0.20190407.1
Author: Research in Theory of Magnetism Department of Taras Shevchenko National University of Kyiv
Author_URI: http://ritm.knu.ua/
Description: Plugin that provides with tools for publication management and ability to auto-fill pages with a list of publications.
*/

define('mpu_plugin_dir', plugin_dir_path(__FILE__)); //defines default plugin directory
//sets some option values that can be changed in settings
add_option('custom_db_prefix','Books_n_Papers');
add_option('upload_dir', 'downloads');
add_option('upload_dir_abs', true);
add_option('previous_db_prefix',get_option('custom_db_prefix'));
add_option('custom_char1_name');
add_option('custom_char1_value');
add_option('custom_char2_name');
add_option('custom_char2_value');
add_option('custom_char3_name');
add_option('custom_char3_value');
add_option('timeout_step',10);
add_option('article_head','<h3>Articles</h3>');
add_option('article_list','[authors] [title] ([year]) [doi] [arxiv] [filelink] [supplementary]');
add_option('conference_head','<h3>Conferences</h3>');
add_option('conference_list','[authors] [title] ([year]) [doi] [arxiv] [filelink] [supplementary]');
add_option('book_head','<h3>Books</h3>');
add_option('book_list','[authors] [title] ([year]) [doi] [arxiv] [filelink] [supplementary]');
add_option('ordered_list','checked');
add_option('list_division','none');
add_option('list_division_style','');
add_option('list_order','DESC');
add_option('drop_db_tables',false);
add_option('author_sort','id');
add_option('author_sort_order', 'ASC');
add_option('paper_sort','date');
add_option('paper_sort_order', 'ASC');

class BooksNPapers //main class
{
	//db table names
	var $dbAuthors;
	var $dbArticles;
	var $dbConferences;
	var $dbBooks;
	var $dbArticleAuthor;
	var $dbConferenceAuthor;
	var $dbBookAuthor;
	var $dbBookEditor;
	var $dbJournals;
	//bradge class that contains dedicated functions
	var $bridge;
	
	function __construct($custom_prefix)
	{
		global $wpdb;
		if(!get_option('drop_db_tables'))
		{
			//setting up table names
			$this->dbAuthors = $wpdb->prefix . $custom_prefix . "_Authors";
			$this->dbArticles = $wpdb->prefix . $custom_prefix . "_Articles";
			$this->dbConferences = $wpdb->prefix . $custom_prefix . "_Conferences";
			$this->dbBooks = $wpdb->prefix . $custom_prefix . "_Books";
			$this->dbArticleAuthor = $wpdb->prefix . $custom_prefix . "_Article_Authors";
			$this->dbConferenceAuthor = $wpdb->prefix . $custom_prefix . "_Conference_Authors";
			$this->dbBookAuthor = $wpdb->prefix . $custom_prefix . "_Book_Authors";
			$this->dbBookEditor = $wpdb->prefix . $custom_prefix . "_Book_Editor";
			$this->dbJournals = $wpdb->prefix . $custom_prefix . "_Journals";
			//setting up tables
			Author::CreateTableAuthors($this->dbAuthors);
			Article::CreateTableArticles($this->dbArticles);
			Conference::CreateTableConferences($this->dbConferences);
			Book::CreateTableBooks($this->dbBooks);
			//setting up bridge
			$this->bridge = new Bridge($this->dbAuthors, $this->dbArticles, $this->dbConferences, $this->dbBooks, $this->dbArticleAuthor, $this->dbConferenceAuthor, $this->dbBookAuthor, $this->dbBookEditor, $this->dbJournals);
			$this->bridge->createAdditionalTables();
			$this->bridge->alterTableCharSet();
			if(get_option('previous_db_prefix')!=get_option('custom_db_prefix'))
			{
				$this->bridge->copyDBInfo();
			}
		}
	}
	
	function printMainAdminPage() //prints content of plugin title page
	{
		include(mpu_plugin_dir . "./pages/title-page.php");
	}
	function printAddAuthorPage() //prints plugin author addition page
	{
		include(mpu_plugin_dir . "./pages/add-author-page.php");
		if(isset($_POST['firstName'])&&isset($_POST['lastName'])) //checks vital inputs for author creation
		{
			$author = new Author($this->dbAuthors); //creates author object and fills data fields
			$author->firstName = $_POST['firstName'];
			$author->lastName = $_POST['lastName'];
			$author->email = $_POST['email'];
			$author->personalUrl = $_POST['url'];
			$author->slug = $_POST['slug'];
			$author->InsertAuthorInfo(); //calls author creation function
			$timeout = get_option('timeout_step') * 100;
			echo "<script>window.onload = function () { document.getElementById('success').innerHTML = 'Author $author->firstName $author->lastName was added.';setTimeout(function() {window.location=document.location.href;},$timeout);  } </script>";
		}
	}
	
	function printManageAuthorsPage() //prints content of author management page
	{
		global $wpdb;
		try{
			$data = $wpdb->get_results("SELECT * FROM " . $this->dbAuthors . " ORDER BY " . get_option('author_sort') . " " . get_option('author_sort_order')); //queries author data from db
			//queries unlisted authors
			$dataAA = $wpdb->get_results("SELECT * FROM " . $this->dbArticleAuthor . " WHERE author_id IS NULL AND author_name IS NOT NULL");
			$dataCA = $wpdb->get_results("SELECT * FROM " . $this->dbConferenceAuthor . "WHERE author_id IS NULL AND author_name IS NOT NULL");
			$dataBA = $wpdb->get_results("SELECT * FROM " . $this->dbBookAuthor . "WHERE author_id IS NULL AND author_name IS NOT NULL");
			$dataBE = $wpdb->get_results("SELECT * FROM " . $this->dbBookEditor . "WHERE editor_id IS NULL AND editor_name IS NOT NULL");
		}catch (Exception $e) {}
		include(mpu_plugin_dir . "./pages/manage-author-page.php");
		if(isset($_POST["checkManage"])) //checks vitals
		{
			$timeout = get_option('timeout_step') * 100;
			if($_POST["checkManage"]=="remove") //checks if remove call was recieved
			{
				if($_POST["answer"]=="yes")
				{ //removes author data from table
					$sql = "DELETE FROM $this->dbAuthors WHERE (id = " . $_POST['authorID'] . ")";
					$wpdb->query($sql);
					echo "<script>window.onload = function () { document.getElementById('remove').innerHTML = 'Author " . $_POST['authorID'] . " has been removed.';setTimeout(function() {window.location=document.location.href;},$timeout); } </script>";
				}
			} else if($_POST["checkManage"]=="update") //checks if modification call was recieved
			{
				$sql = "UPDATE $this->dbAuthors SET first_name = '" . $_POST["firstName"] . "', last_name = '" . $_POST["lastName"] . "', email = '" . $_POST["email"] . "', personal_url = '" . $_POST["url"] . "', slug = '" . $_POST["slug"] . "' WHERE (id = " . $_POST["authorID"] . ")"; //updates author data in table
				$wpdb->query($sql);
				echo "<script>window.onload = function () { document.getElementById('success').innerHTML = 'Author " . $_POST['authorID'] . " has been updated.';setTimeout(function() {window.location=document.location.href;},$timeout); }</script>";
			}
		}
		if(isset($_POST["mergeList1"]))
		{
			$timeout = get_option('timeout_step') * 100;
			$counter=1;
			while($counter<=$_POST["numberOfAuthors"])
			{
				if($_POST["mergeAuthorId"]!=$_POST["mergeList" . $counter])
				{
					$this->bridge->mergeAuthors($_POST["mergeAuthorId"], $_POST["mergeList" . $counter]);
				}
				$counter++;
			}
			echo "<script>window.onload = function () { document.getElementById('success').innerHTML = 'Author " . $_POST['mergeAuthorId'] . " has been updated.';setTimeout(function() {window.location=document.location.href;},$timeout); }</script>";
		}
		if(isset($_POST["sortMethod"]))
		{
			update_option('author_sort',$_POST["sortMethod"]);
			update_option('author_sort_order',$_POST["sortMethodOrder"]);
			echo "<script>window.location=document.location.href;</script>";
		}
		if(isset($_POST["promote"]))
		{
			$author = new Author($this->dbAuthors); //creates author object and fills data fields
			$author->firstName = $_POST['firstName'];
			$author->lastName = $_POST['lastName'];
			$author->email = $_POST['email'];
			$author->personalUrl = $_POST['url'];
			$author->slug = $_POST['slug'];
			$author->InsertAuthorInfo(); //calls author creation function
			$newAuthor = $wpdb->get_results("SELECT * FROM $this->dbAuthors ORDER BY id DESC LIMIT 1");
			foreach($newAuthor as $na)
			{
				if($_POST["relation"]=="aa")
				{
					$sql = "UPDATE $this->dbArticleAuthor SET author_name = NULL, author_id = $na->id WHERE (id = " . $_POST["promote"] . ")"; //updates author data in table
					$wpdb->query($sql);
				} else if ($_POST["relation"]=="ca")
				{
					$sql = "UPDATE $this->dbConferenceAuthor SET author_name = NULL, author_id = $na->id WHERE (id = " . $_POST["promote"] . ")"; //updates author data in table
					$wpdb->query($sql);
				} else if($_POST["relation"]=="ba")
				{
					$sql = "UPDATE $this->dbBookAuthor SET author_name = NULL, author_id = $na->id WHERE (id = " . $_POST["promote"] . ")"; //updates author data in table
					$wpdb->query($sql);
				} else
				{
					$sql = "UPDATE $this->dbEditorAuthor SET editor_name = NULL, editor_id = $na->id WHERE (id = " . $_POST["promote"] . ")"; //updates author data in table
					$wpdb->query($sql);
				}
			}
			$timeout = get_option('timeout_step') * 100;
			echo "<script>window.onload = function () { document.getElementById('success').innerHTML = 'Author $author->firstName $author->lastName was added.';setTimeout(function() {window.location=document.location.href;},$timeout);  } </script>";
		}
	}
	
	function printAddPaperPage() //prints content of paper addition page
	{
		global $wpdb;
		try{
			$data = $wpdb->get_results("SELECT * FROM " . $this->dbAuthors . " ORDER BY last_name,first_name ASC"); //queries author data from db
			$option_values = ""; //presets datalist inner content
			foreach($data as $author) //fills datalist inner content
			{
				$option_values = $option_values . "<option label=\"(Internal) $author->first_name $author->last_name\" value=\"$author->first_name $author->last_name\"></option>";
			}
			$data = $wpdb->get_results("SELECT * FROM " . $this->dbJournals . " ORDER BY journal ASC"); //queries journal data from db
			$j_option_values = ""; //presets datalist inner content
			foreach($data as $journal) //fills datalist inner content
			{
				$j_option_values = $j_option_values . "<option label=\"(Internal)\" value=\"$journal->journal\"></option>";
			}
		}catch (Exception $e) {}
		include(mpu_plugin_dir . "./pages/add-paper-page.php");
		if(isset($_POST["title"])) //checks vital
		{
			//sets temporal values of custom characteristics
			$char1val = NULL;
			$char2val = NULL;
			$char3val = NULL;
			//checks if custom characteristics were set
			if(isset($_POST["char1"]))
			{
				$char1val = $_POST["char1"]; //if true, sets submitted values to variables
			}
			if(isset($_POST["char2"]))
			{
				$char2val = $_POST["char2"];
			}
			if(isset($_POST["char3"]))
			{
				$char3val = $_POST["char3"];
			}
			if($_POST["paperType"]=="article") //inserts article paper info
			{
				$fileName = "";
				if(isset($_POST["file_m"]) && $_POST["file_m"]!="")
					$fileName = $_POST["file_m"];
				else
					$fileName = $_FILES['file']['name'];
				$this->bridge->addArticle($_POST["title"], $_POST["year"], $_POST["pages"], $_POST["doi"], $_POST["url"], $_POST["issn"], $_POST["supp"], $fileName, $_POST["date"], $_POST["public"], $_POST["arxiv"], $this->bridge->checkJournal($_POST["journal"]), $_POST["volume"], $_POST["issue"], $char1val, $char2val, $char3val, $_POST["preprint"]); //writes article data in table
				$art_id;
				$data = $wpdb->get_results("SELECT id FROM " . $this->dbArticles . " ORDER By id DESC LIMIT 1"); //gets id of previously created article
				foreach($data as $el)
				{
					$art_id = $el->id; //sets queried value to variable
				}
				for($k = 0; $k <= 20; $k++)
				{
					if(isset($_POST["author" . $k]))
					{ //relates every author with article
						$this->bridge->addArticleAuthor($this->bridge->checkAuthor($_POST["author" . $k]), $art_id);
					}
				}
				if(isset($_FILES['file']) && $_FILES['file']['name'] != "")
				{
					$uploaddir = "";
					if (get_option('upload_dir_abs'))
					{
						$uploaddir .= ABSPATH;
					}
					$uploaddir .= get_option('upload_dir');
					if (!is_dir($uploaddir))
					{
						mkdir($uploaddir);
					}
					$uploadfile = $uploaddir . '/' . basename($_FILES['file']['name']);
					if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
						echo "File was successfully uploaded.\n";
					} else {
						echo "File upload has failed.\n";
					}
				}
			} else if($_POST["paperType"]=="conference") //inserts conference info
			{
				$fileName = "";
				if(isset($_POST["file_m"]) && $_POST["file_m"]!="")
					$fileName = $_POST["file_m"];
				else
					$fileName = $_FILES['file']['name'];
				$this->bridge->addConference($_POST["title"], $_POST["year"], $_POST["pages"], $_POST["doi"], $_POST["url"], $_POST["issn"], $_POST["supp"], $fileName, $_POST["date"], $_POST["public"], $_POST["arxiv"], $_POST["bookTitle"], $_POST["confPages"], $char1val, $char2val, $char3val); //writes conference data in table
				$art_id;
				$data = $wpdb->get_results("SELECT id FROM " . $this->dbConferences . " ORDER By id DESC LIMIT 1"); //gets id of previously created conference
				foreach($data as $el)
				{
					$art_id = $el->id; //sets queried value to variable
				}
				for($k = 0; $k <= 20; $k++)
				{
					if(isset($_POST["author" . $k]))
					{ //relates every author with conference
						$this->bridge->addConferenceAuthor($this->bridge->checkAuthor($_POST["author" . $k]), $art_id);
					}
				}
				if(isset($_FILES['file']) && $_FILES['file']['name'] != "")
				{
					$uploaddir = "";
					if (get_option('upload_dir_abs'))
					{
						$uploaddir .= ABSPATH;
					}
					$uploaddir .= get_option('upload_dir');
					if (!is_dir($uploaddir))
					{
						mkdir($uploaddir);
					}
					$uploadfile = $uploaddir . '/' . basename($_FILES['file']['name']);
					if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
						echo "File was successfully uploaded.\n";
					} else {
						echo "File upload has failed.\n";
					}
				}
			} else if($_POST["paperType"]=="book") //inserts book info
			{
				$fileName = "";
				if(isset($_POST["file_m"]) && $_POST["file_m"]!="")
					$fileName = $_POST["file_m"];
				else
					$fileName = $_FILES['file']['name'];
				$this->bridge->addBook($_POST["title"], $_POST["year"], $_POST["pages"], $_POST["doi"], $_POST["url"], $_POST["issn"], $_POST["supp"], $fileName, $_POST["date"], $_POST["public"], $_POST["arxiv"], $_POST["publisher"], $_POST["chapter"], $_POST["isbn"], $char1val, $char2val, $char3val); //writes book data in table
				$art_id;
				$data = $wpdb->get_results("SELECT id FROM " . $this->dbBooks . " ORDER By id DESC LIMIT 1"); //gets id of previously created book
				foreach($data as $el)
				{
					$art_id = $el->id; //sets queried value to variable
				}
				for($k = 0; $k <= 20; $k++)
				{
					if(isset($_POST["author" . $k]))
					{ //relates every author with book
						$this->bridge->addBookAuthor($this->bridge->checkAuthor($_POST["author" . $k]), $art_id);
					}
				}
				for($k = 0; $k <= 20; $k++)
				{
					if(isset($_POST["editor" . $k]))
					{ //relates every editor with book
						$this->bridge->addBookEditor($this->bridge->checkAuthor($_POST["editor" . $k]), $art_id);
					}
				}
				if(isset($_FILES['file']) && $_FILES['file']['name'] != "")
				{
					$uploaddir = "";
					if (get_option('upload_dir_abs'))
					{
						$uploaddir .= ABSPATH;
					}
					$uploaddir .= get_option('upload_dir');
					if (!is_dir($uploaddir))
					{
						mkdir($uploaddir);
					}
					$uploadfile = $uploaddir . '/' . basename($_FILES['file']['name']);
					if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
						echo "File was successfully uploaded.\n";
					} else {
						echo "File upload has failed.\n";
					}
				}
			}
			$timeout = get_option('timeout_step') * 100;
			//echo "<script>window.onload = function () { document.getElementById('success').innerHTML = 'Publication " . $_POST["title"] . " was added.';setTimeout(function() {window.location=document.location.href;},$timeout);  } </script>";
		}
	}
	
	function printManagePapersPage() //prints content of paper management page
	{
		global $wpdb;
		try{
			$data = $wpdb->get_results("SELECT * FROM " . $this->dbArticles . " ORDER BY " . get_option('paper_sort') . " " . get_option('paper_sort_order')); //queries article data from db
			$data5 = $wpdb->get_results("SELECT * FROM " . $this->dbAuthors . " ORDER BY last_name ASC"); //queries author data from db
			$option_values = ""; //presets datalist inner content
			foreach($data5 as $author) //fills datalist inner content
			{
				$option_values = $option_values . "<option label=\'(Internal) $author->first_name $author->last_name\' value=\'$author->first_name $author->last_name\'></option>";
			}
			$data5 = $wpdb->get_results("SELECT * FROM " . $this->dbJournals . " ORDER BY journal ASC"); //queries journal data from db
			$j_option_values = ""; //presets datalist inner content
			foreach($data5 as $journal) //fills datalist inner content
			{
				$j_option_values = $j_option_values . "<option label=\"(Internal)\" value=\"$journal->journal\"></option>";
			}
			$dataC = $wpdb->get_results("SELECT * FROM " . $this->dbConferences . " ORDER BY " . get_option('paper_sort') . " " . get_option('paper_sort_order')); //queries conference data from db
			$dataB = $wpdb->get_results("SELECT * FROM " . $this->dbBooks . " ORDER BY " . get_option('paper_sort') . " " . get_option('paper_sort_order')); //queries book data from db
		}catch (Exception $e) {}
		include(mpu_plugin_dir . "./pages/manage-papers-page.php");
		if(isset($_POST["checkManage"])) //checks vitals
		{
			if(isset($_POST["journal"])) //if article
			{
				if($_POST["checkManage"]=="remove") //remove call
				{
					$this->bridge->removeArticle($_POST["answer"], $_POST['articleID']);
				}
				else if($_POST["checkManage"]=="update") //update call
				{
					$wpdb->query("DELETE FROM $this->dbArticleAuthor WHERE (article_id = " . $_POST['articleID'] . ")"); //clears all authors related to article
					for($k=0;$k<=20;$k++)
					{
						if(isset($_POST["author" . $k]) && $_POST["author" . $k]!="")
						{ //creates new relations between article and authors
							$this->bridge->addArticleAuthor($this->bridge->checkAuthor($_POST["author" . $k]), $_POST['articleID']);
						}
					}
					$char1val = "";
					$char2val = "";
					$char3val = "";
					if(isset($_POST["char1"]))
					{
						$char1val = $_POST["char1"];
					}
					if(isset($_POST["char2"]))
					{
						$char2val = $_POST["char2"];
					}
					if(isset($_POST["char3"]))
					{
						$char3val = $_POST["char3"];
					}
					$fileName = "";
					if(isset($_POST["file_m"]) && $_POST["file_m"]!="")
						$fileName = $_POST["file_m"];
					else
						$fileName = $_FILES['file']['name'];
					$this->bridge->manageArticle($_POST["title"], $_POST["year"], $_POST["pages"], $_POST["doi"], $_POST["url"], $_POST["issn"], $_POST["supp"], $fileName, $_POST["date"], $_POST["public"], $_POST["arxiv"], $this->bridge->checkJournal($_POST["journal"]), $_POST["volume"], $_POST["issue"], $_POST["articleID"], $char1val, $char2val, $char3val, $_POST["preprint"]);
					if(isset($_FILES['file']) && $_FILES['file']['name'] != "")
					{
						$uploaddir = "";
						if (get_option('upload_dir_abs'))
						{
							$uploaddir .= ABSPATH;
						}
						$uploaddir .= get_option('upload_dir');
						if (!is_dir($uploaddir))
						{
							mkdir($uploaddir);
						}
						$uploadfile = $uploaddir . '/' . basename($_FILES['file']['name']);
						if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
							echo "File was successfully uploaded.\n";
						} else {
							echo "File upload has failed.\n";
						}
					}
					if(isset($_POST["file_d"]) && $_POST["file_d"]=="true" && $_POST["file_d_name"] != "")
					{
						$this->bridge->deleteFile($_POST["file_d_name"]);
					}
				}
			}
			else if(isset($_POST["bookTitle"])) //if conference
			{
				if($_POST["checkManage"]=="remove") //remove call
				{
					$this->bridge->removeConference($_POST["answer"], $_POST['articleID']);
				}
				else if($_POST["checkManage"]=="update") //update call
				{
					$wpdb->query("DELETE FROM $this->dbConferenceAuthor WHERE (conference_id = " . $_POST['articleID'] . ")");
					for($k=0;$k<=20;$k++)
					{
						if(isset($_POST["author" . $k]) && $_POST["author" . $k]!="")
						{
							$this->bridge->addConferenceAuthor($this->bridge->checkAuthor($_POST["author" . $k]), $_POST['articleID']);
						}
					}
					$char1val = "";
					$char2val = "";
					$char3val = "";
					if(isset($_POST["char1"]))
					{
						$char1val = $_POST["char1"];
					}
					if(isset($_POST["char2"]))
					{
						$char2val = $_POST["char2"];
					}
					if(isset($_POST["char3"]))
					{
						$char3val = $_POST["char3"];
					}
					$fileName = "";
					if(isset($_POST["file_m"]) && $_POST["file_m"]!="")
						$fileName = $_POST["file_m"];
					else
						$fileName = $_FILES['file']['name'];
					$this->bridge->manageConference($_POST["title"], $_POST["year"], $_POST["pages"], $_POST["doi"], $_POST["url"], $_POST["issn"], $_POST["supp"], $fileName, $_POST["date"], $_POST["public"], $_POST["arxiv"], $_POST["bookTitle"], $_POST["confPages"], $_POST["articleID"], $char1val, $char2val, $char3val);
					if(isset($_FILES['file']) && $_FILES['file']['name'] != "")
					{
						$uploaddir = "";
						if (get_option('upload_dir_abs'))
						{
							$uploaddir .= ABSPATH;
						}
						$uploaddir .= get_option('upload_dir');
						if (!is_dir($uploaddir))
						{
							mkdir($uploaddir);
						}
						$uploadfile = $uploaddir . '/' . basename($_FILES['file']['name']);
						if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
							echo "File was successfully uploaded.\n";
						} else {
							echo "File upload has failed.\n";
						}
					}
					if(isset($_POST["file_d"]) && $_POST["file_d"]=="true" && $_POST["file_d_name"] != "")
					{
						$this->bridge->deleteFile($_POST["file_d_name"]);
					}
				}
			}
			else if(isset($_POST["publisher"])) //if book
			{
				if($_POST["checkManage"]=="remove") //remove call
				{
					$this->bridge->removeBook($_POST["answer"], $_POST['articleID']);
				}
				else if($_POST["checkManage"]=="update") //update call
				{
					$wpdb->query("DELETE FROM $this->dbBookAuthor WHERE (book_id = " . $_POST['articleID'] . ")");
					for($k=0;$k<=20;$k++)
					{
						if(isset($_POST["author" . $k]) && $_POST["author" . $k]!="")
						{
							$this->bridge->addBookAuthor($this->bridge->checkAuthor($_POST["author" . $k]), $_POST['articleID']);
						}
					}
					$wpdb->query("DELETE FROM $this->dbBookEditor WHERE (book_id = " . $_POST['articleID'] . ")");
					for($k=0;$k<=20;$k++)
					{
						if(isset($_POST["editor" . $k]) && $_POST["editor" . $k]!="")
						{
							$this->bridge->addBookEditor($this->bridge->checkAuthor($_POST["editor" . $k]), $_POST['articleID']);
						}
					}
					$char1val = "";
					$char2val = "";
					$char3val = "";
					if(isset($_POST["char1"]))
					{
						$char1val = $_POST["char1"];
					}
					if(isset($_POST["char2"]))
					{
						$char2val = $_POST["char2"];
					}
					if(isset($_POST["char3"]))
					{
						$char3val = $_POST["char3"];
					}
					$fileName = "";
					if(isset($_POST["file_m"]) && $_POST["file_m"]!="")
						$fileName = $_POST["file_m"];
					else
						$fileName = $_FILES['file']['name'];
					$this->bridge->manageBook($_POST["title"], $_POST["year"], $_POST["pages"], $_POST["doi"], $_POST["url"], $_POST["issn"], $_POST["supp"], $fileName, $_POST["date"], $_POST["public"], $_POST["arxiv"], $_POST["publisher"], $_POST["chapter"], $_POST["isbn"], $_POST["articleID"], $char1val, $char2val, $char3val);
					if(isset($_FILES['file']) && $_FILES['file']['name'] != "")
					{
						$uploaddir = "";
						if (get_option('upload_dir_abs'))
						{
							$uploaddir .= ABSPATH;
						}
						$uploaddir .= get_option('upload_dir');
						if (!is_dir($uploaddir))
						{
							mkdir($uploaddir);
						}
						$uploadfile = $uploaddir . '/' . basename($_FILES['file']['name']);
						if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
							echo "File was successfully uploaded.\n";
						} else {
							echo "File upload has failed.\n";
						}
					}
					if(isset($_POST["file_d"]) && $_POST["file_d"]=="true" && $_POST["file_d_name"] != "")
					{
						$this->bridge->deleteFile($_POST["file_d_name"]);
					}
				}
			}
		}
		if(isset($_POST["sortMethod"]))
		{
			update_option('paper_sort',$_POST["sortMethod"]);
			update_option('paper_sort_order',$_POST["sortMethodOrder"]);
			echo "<script>window.location=document.location.href;</script>";
		}
	}
	
	function printImportDataPage() //prints content of data import page
	{
		global $wpdb;
		try{
			$data = $wpdb->get_results("SELECT * FROM " . $this->dbAuthors . " ORDER BY last_name,first_name ASC"); //queries author data from db
			$option_values = ""; //presets datalist inner content
			foreach($data as $author) //fills datalist inner content
			{
				$option_values = $option_values . "<option label=\"(Internal) $author->first_name $author->last_name\" value=\"$author->first_name $author->last_name\"></option>";
			}
			$data = $wpdb->get_results("SELECT * FROM " . $this->dbJournals . " ORDER BY journal ASC"); //queries journal data from db
			$j_option_values = ""; //presets datalist inner content
			foreach($data as $journal) //fills datalist inner content
			{
				$j_option_values = $j_option_values . "<option label=\"(Internal)\" value=\"$journal->journal\"></option>";
			}
			$newAuthors = "<script>window.onload = function () { document.getElementById('bibInputs').innerHTML = 'Next Authors were added: "; //presets function that shows which new authors were imported
		}catch (Exception $e) {}
		include(mpu_plugin_dir . "./pages/import-data-page.php");
		if(isset($_POST["doiUpload"]) && $_POST["doiUpload"] != "")
		{
			$result = $this->bridge->requestDataByDOI($_POST["doiUpload"]);
			if($result == 'Document Not Found')
			{
				echo "<script>window.onload = function() { document.getElementById('doiInputFields').innerHTML = '" . $result . "';}</script>";
			} else
			{
				echo "<script>window.onload = function() { document.getElementById('importDoiSubmit').innerHTML = '';document.getElementById('importDoiSubmit').style = 'display:none';document.getElementById('doiInputHide').innerHTML = '';document.getElementById('doiInputFields').innerHTML = '" . $this->bridge->handleDOIData($result, $j_option_values, $option_values) . "';doiFillDataFields();}</script>";
			}
		}
		if(isset($_POST["paperType"]))
		{
			//sets temporal values of custom characteristics
			$char1val = NULL;
			$char2val = NULL;
			$char3val = NULL;
			//checks if custom characteristics were set
			if(isset($_POST["char1"]))
			{
				$char1val = $_POST["char1"]; //if true, sets submitted values to variables
			}
			if(isset($_POST["char2"]))
			{
				$char2val = $_POST["char2"];
			}
			if(isset($_POST["char3"]))
			{
				$char3val = $_POST["char3"];
			}
			if($_POST["paperType"]=="article") //inserts article paper info
			{
				$this->bridge->addArticle($_POST["title"], $_POST["year"], $_POST["pages"], $_POST["doi"], $_POST["url"], $_POST["issn"], $_POST["supp"], $_POST["file"], $_POST["date"], $_POST["public"], $_POST["arxiv"], $this->bridge->checkJournal($_POST["journal"]), $_POST["volume"], $_POST["issue"], $char1val, $char2val, $char3val, $_POST["preprint"]); //writes article data in table
				$art_id;
				$data = $wpdb->get_results("SELECT id FROM " . $this->dbArticles . " ORDER By id DESC LIMIT 1"); //gets id of previously created article
				foreach($data as $el)
				{
					$art_id = $el->id; //sets queried value to variable
				}
				for($k = 0; $k <= 20; $k++)
				{
					if(isset($_POST["author" . $k]))
					{ //relates every author with article
						$this->bridge->addArticleAuthor($this->bridge->checkAuthor($_POST["author" . $k]), $art_id);
					}
				}
			} else if($_POST["paperType"]=="conference") //inserts conference info
			{
				$this->bridge->addConference($_POST["title"], $_POST["year"], $_POST["pages"], $_POST["doi"], $_POST["url"], $_POST["issn"], $_POST["supp"], $_POST["file"], $_POST["date"], $_POST["public"], $_POST["arxiv"], $_POST["bookTitle"], $_POST["confPages"], $char1val, $char2val, $char3val); //writes conference data in table
				$art_id;
				$data = $wpdb->get_results("SELECT id FROM " . $this->dbConferences . " ORDER By id DESC LIMIT 1"); //gets id of previously created conference
				foreach($data as $el)
				{
					$art_id = $el->id; //sets queried value to variable
				}
				for($k = 0; $k <= 20; $k++)
				{
					if(isset($_POST["author" . $k]))
					{ //relates every author with conference
						$this->bridge->addConferenceAuthor($this->bridge->checkAuthor($_POST["author" . $k]), $art_id);
					}
				}
			} else if($_POST["paperType"]=="book") //inserts book info
			{
				$this->bridge->addBook($_POST["title"], $_POST["year"], $_POST["pages"], $_POST["doi"], $_POST["url"], $_POST["issn"], $_POST["supp"], $_POST["file"], $_POST["date"], $_POST["public"], $_POST["arxiv"], $_POST["publisher"], $_POST["chapter"], $_POST["isbn"], $char1val, $char2val, $char3val); //writes book data in table
				$art_id;
				$data = $wpdb->get_results("SELECT id FROM " . $this->dbBooks . " ORDER By id DESC LIMIT 1"); //gets id of previously created book
				foreach($data as $el)
				{
					$art_id = $el->id; //sets queried value to variable
				}
				for($k = 0; $k <= 20; $k++)
				{
					if(isset($_POST["author" . $k]))
					{ //relates every author with book
						$this->bridge->addBookAuthor($this->bridge->checkAuthor($_POST["author" . $k]), $art_id);
					}
				}
				for($k = 0; $k <= 20; $k++)
				{
					if(isset($_POST["editor" . $k]))
					{ //relates every editor with book
						$this->bridge->addBookEditor($this->bridge->checkAuthor($_POST["editor" . $k]), $art_id);
					}
				}
			}
			$timeout = get_option('timeout_step') * 100;
			echo "<script>window.onload = function () { document.getElementById('doiInputFields').innerHTML = 'Publication " . $_POST["title"] . " was added.';setTimeout(function() {window.location=document.location.href;},$timeout);  } </script>";
		}
		if(isset($_POST["bibUpload"]))
		{
			for($k = 0; $k<$_POST["k"]; $k++) //for each imported publication
			{
				if(isset($_POST["article" . $k]))
				{
					$newAuthors .= $this->bridge->importArticle(mb_convert_encoding($_POST["article" . $k],'Windows-1251'));//imports article
				} else if(isset($_POST["book" . $k]))
				{
					$newAuthors .= $this->bridge->importBook(mb_convert_encoding($_POST["book" . $k],'Windows-1251'));//imports book
				} else if(isset($_POST["conference" . $k]))
				{
					$newAuthors .= $this->bridge->importConference(mb_convert_encoding($_POST["conference" . $k],'Windows-1251'));//imports conference
				}
			}
			if($newAuthors == "<script>window.onload = function () { document.getElementById('bibInputs').innerHTML = 'Next Authors were added: ") //checks if any new authors were added
			{
				echo "<script>window.onload = function () { document.getElementById('bibInputs').innerHTML = 'No new Authors were added.';setTimeout(function() {window.location=document.location.href;},$timeout);} </script>"; //if NOT shows that no new authors were added
			} else
			{
				$newAuthors .= "';setTimeout(function() {window.location=document.location.href;},$timeout); } </script>"; //if TRUE shows which authors were added
				echo $newAuthors;
			}
		}
	}
	
	function printSettingsPage() //prints content of settings sub-page
	{
		global $wpdb;
		include(mpu_plugin_dir . "./pages/settings-page.php");
		if(isset($_POST["prefix"])) //checks if db prefix was set
		{
			if($_POST["prefix"] != "") //if not empty, sets new prefix value
			{
				update_option('custom_db_prefix',$_POST["prefix"]);
				echo "<script type='text/javascript'> window.location=document.location.href;</script>";
			}
		}
		//sets file upload directory
		if(isset($_POST["upload_abs_hidden"]))
		{
			update_option('upload_dir',$_POST["upload"]);
			if(isset($_POST["upload_abs"]))
				update_option('upload_dir_abs',true);
			else
				update_option('upload_dir_abs',false);
			echo "<script type='text/javascript'> window.location=document.location.href;</script>";
		}
		//sets publication list options
		if(isset($_POST["article_list"]))
		{
			update_option('article_head',$_POST["article_head"]);
			update_option('article_list',$_POST["article_list"]);
			update_option('conference_head',$_POST["conference_head"]);
			update_option('conference_list',$_POST["conference_list"]);
			update_option('book_head',$_POST["book_head"]);
			update_option('book_list',$_POST["book_list"]);
			if(isset($_POST["orderedList"]))
			{
				update_option('ordered_list','checked');
			} else
			{
				update_option('ordered_list','');
			}
			update_option('list_division',$_POST["listDivision"]);
			update_option('list_division_style',$_POST["listDivisionStyle"]);
			update_option('list_order',$_POST["listOrder"]);
			echo "<script type='text/javascript'> window.location=document.location.href;</script>";
		}
		//sets characteristics values
		if(isset($_POST["char1name"]))
		{
			update_option('custom_char1_name',$_POST["char1name"]);
			update_option('custom_char1_value',$_POST["char1value"]);
			echo "<script type='text/javascript'> window.location=document.location.href;</script>";
		}
		if(isset($_POST["char2name"]))
		{
			update_option('custom_char2_name',$_POST["char2name"]);
			update_option('custom_char2_value',$_POST["char2value"]);
			echo "<script type='text/javascript'> window.location=document.location.href;</script>";
		}
		if(isset($_POST["char3name"]))
		{
			update_option('custom_char3_name',$_POST["char3name"]);
			update_option('custom_char3_value',$_POST["char3value"]);
			echo "<script type='text/javascript'> window.location=document.location.href;</script>";
		}
		if(isset($_POST["timestep"]))
		{
			update_option('timeout_step',$_POST["timestep"]);
			echo "<script type='text/javascript'> window.location=document.location.href;</script>";
		}
		if(isset($_POST["drop_db"]))
		{
			if(get_option('drop_db_tables'))
			{
				update_option('drop_db_tables',false);
			} else
			{
				$this->bridge->dropTables(get_option('custom_db_prefix'));
				update_option('drop_db_tables',true);
				update_option('db_import_debug', 'No logs');
			}
			echo "<script type='text/javascript'> window.location=document.location.href;</script>";
		}
	}
}

//load additional classes
include(mpu_plugin_dir . "./classes/author.php");
include(mpu_plugin_dir . "./classes/item.php");
include(mpu_plugin_dir . "./classes/article.php");
include(mpu_plugin_dir . "./classes/conference.php");
include(mpu_plugin_dir . "./classes/book.php");
include(mpu_plugin_dir . "./classes/bridge.php");

//create object of MyPapersUpdated class
$bnp = new BooksNPapers(get_option('custom_db_prefix', '_Books_n_Papers_'));
function BNPAdminPage() //creates admin menu page
{
	global $bnp;
	if (function_exists('add_menu_page'))
	{ //hooks admin menu pages
		add_menu_page('Books and Papers', 'Books and Papers', 'publish_pages', 'books-n-papers.php', array(&$bnp, 'printMainAdminPage'), 'dashicons-welcome-add-page', 7);
		add_submenu_page('books-n-papers.php', 'Add Author', 'Add Author', 'publish_pages', 'add-author-u.php', array(&$bnp, 'printAddAuthorPage'));
		add_submenu_page('books-n-papers.php', 'Manage Authors', 'Manage Authors', 'publish_pages', 'manage-authors-u.php', array(&$bnp, 'printManageAuthorsPage'));
		add_submenu_page('books-n-papers.php', 'Add Work', 'Add Work', 'publish_pages', 'add-paper-u.php', array(&$bnp, 'printAddPaperPage'));
		add_submenu_page('books-n-papers.php', 'Manage Works', 'Manage Works', 'publish_pages', 'manage-papers-u.php', array(&$bnp, 'printManagePapersPage'));
		add_submenu_page('books-n-papers.php', 'Import Works', 'Import Works', 'publish_pages', 'import-papers-u.php', array(&$bnp, 'printImportDataPage'));
		add_options_page('Books and Papers', 'Books and Papers', 'publish_pages', 'paper-settings-u.php', array(&$bnp, 'printSettingsPage'));
	}
}
add_action('admin_menu','BNPAdminPage'); //hooks plugin menu page to admin menu
//adds filters to fill tagged text with data
if(!get_option('drop_db_tables'))
{
	add_filter('the_content', array($bnp->bridge ,'replacePublications'));
	add_filter('the_content', array($bnp->bridge ,'replaceAuthorName'));
}
?>